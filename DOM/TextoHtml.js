const $whatIsDOM = document.getElementById("que-es");

let text = `
<p>
 El modelo de Objetos del documento (<b><i>DOM - Document Object Model </i></b>) es un API para documentos HTML y XML.
</p>
<p>
 Éste proveé una representación estructural del documento, permitiendo modificar su contenido y presentación visual mediante código JS.
</p>
<p>
 <mark>
  El DOM no es parte de la especificación de javascript, es una API para los navegadores
 </mark>
</p>
`;

// El metodo no estandar para reemplazar texto es innerText (Pero no soporta etiquetas HTML)
$whatIsDOM.innerText = text;

// El metodo estandar para reemplazar texto es textcontent (Tampoco soporta etiquetas HTML)
$whatIsDOM.textContent = text;

// El metodo estandar para reemplazar contenido y soporta etiquetas HTML es innerHTML
$whatIsDOM.innerHTML = text;

// El metodo outerHTML permite eliminar todo el contenido(Incluyendo etiquetas HTML e insertar el texto con etiquetas html)
$whatIsDOM.outerHTML = text;