const $cards = document.querySelector(".cards");

console.log($cards);

console.log($cards.children);
// Obtiene el segundo hijo de cards
console.log($cards.children[2]);
// Obtiene el padre de cards
console.log($cards.parentElement);
// Obtiene el primer hijo
console.log($cards.firstElementChild);
// Obtiene el ultimo hijo
console.log($cards.lastElementChild);

// retorna el Element predecesor inmediato al especificado dentro de la lista de hijos de su padre, 
// o bien null si el elemento especificado es el primero de dicha lista.
console.log($cards.previousElementSibling);

console.log($cards.nextElementSibling);
/**
 * El método closest() de la interfaz Element devuelve el ascendiente más cercano al elemento actual
 */
console.log($cards.closest("div"));
console.log($cards.closest("body"));
console.log($cards.children[3].closest("section"));
