// Para acceder a atributos HTML se utiliza el atributo como tal
// Modo Uno
console.log(document.documentElement.lang);

// Modo dos
console.log(document.documentElement.getAttribute("lang"));

// Diferencia 
console.log(document.querySelector(".link-dom").href);


console.log(document.querySelector(".link-dom").getAttribute("href"));

// Como cambiar atributos 

// Modo uno
document.documentElement.lang = "es";

console.log(document.documentElement.lang);

// Modo Dos

document.documentElement.setAttribute("lang","es-MX");

console.log(document.documentElement.lang);

// Es considerado una buena practica las variables donde se almacenen temas del dom se escriban en signo $ por delante
// Metodo de modificación almacenando en variable

const $linkDOM = document.querySelector(".link-dom");

$linkDOM.setAttribute("target","_blank");
// Buena practica cuando se abre una nueva ventana para que no se considere insegura 
$linkDOM.setAttribute("rel","noopener");

$linkDOM.setAttribute("href","https://youtube.com/jonmircha");


// Para verificar si tiene un atributo o no se ocupa el has

console.log($linkDOM.hasAttribute("rel"));

// Para remover atributos se utiliza el removeAttribute

$linkDOM.removeAttribute("rel");

console.log($linkDOM.hasAttribute("rel"));

// Data-Attributes

console.log($linkDOM.getAttribute("data-description"));

// Obtiene todos los data del elemento
console.log($linkDOM.dataset);

// Para cambiar valores de atributos data

$linkDOM.setAttribute("data-description","Modelo de Objeto del Documento");

console.log($linkDOM.dataset.description);