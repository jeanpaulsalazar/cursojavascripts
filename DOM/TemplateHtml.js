const $cards = document.querySelector(".cards"),
$template = document.getElementById("template-card").content,
$fragment = document.createDocumentFragment(),
cardContent=[
    {
        title:"tecnologia",
        img:"https://placeimg.com/200/200/tech",
    },
    {
        title:"Animales",
        img:"https://placeimg.com/200/200/animals",
    },
    {
        title:"tecnologia",
        img:"https://placeimg.com/200/200/arch",
    },
    {
        title:"tecnologia",
        img:"https://placeimg.com/200/200/people",
    },
    {
        title:"Naturaleza",
        img:"https://placeimg.com/200/200/nature",
    },
    
    

];

cardContent.forEach(el=>{
    $template.querySelector("img").setAttribute("src",el.img);
    $template.querySelector("img").setAttribute("alt",el.title);
    $template.querySelector("figcaption").textContent= el.title;

    // Para clonar un nodo completo del html se utiliza la propiedad importNode (si se define el true el segundo parametro copiara todas sus estructuras internas)
    let $clone = document.importNode($template,true);
    $fragment.appendChild($clone);
});

//Agregamos al dom todo el fragment
$cards.appendChild($fragment);