const $card = document.querySelector(".card");
console.log($card);
console.log($card.className);
console.log($card.classList);
// verifica si existe algúna clase rotate-45
console.log($card.classList.contains("rotate-45"));
// Seteamos rotate 45 al primer elemento de card
$card.classList.add("rotate-45");

console.log($card.classList.contains("rotate-45"));

console.log($card.className);
console.log($card.classList);

// Permite remover una clase
$card.classList.remove("rotate-45");
console.log($card.classList.contains("rotate-45"));

// el metodo toggle funciona como interrutor si esta apagado lo enciende y si esta encendido lo apaga(Si no lo posee se lo agrega y si lo posee se lo quita)
$card.classList.toggle("rotate-45")
console.log($card.classList.contains("rotate-45"));
$card.classList.toggle("rotate-45")
console.log($card.classList.contains("rotate-45"));

// para reemplazar una clase por otra se utiliza replace
$card.classList.toggle("rotate-45")
$card.classList.replace("rotate-45","rotate-135")

// es posible agregar mas de una clase 
$card.classList.add("opacity-80","sepia")
