function Animal(nombre,genero){
    //Atributo
    this.nombre = nombre;
    this.genero = genero;  
}
    // Metodo
Animal.prototype.sonar = function(){
    console.log("Hago sonidos");    
}

// Herencia Prototipica
function  Perro(nombre,genero,tamano){
    this.super = Animal;
    this.super(nombre,genero);
    this.tamano;
}

// Perro está heredando de Animal

Perro.prototype = new  Animal();
Perro.prototype.constructor  = Perro;

// Sobreescritura de métodos del prototipo padre en el hijo

Perro.prototype.sonar  =  function(){
    console.log('Soy un perro y mi sonido es un ladrido');
}

Perro.prototype.ladrar = function(){
    console.log('Guauuuu  Guauuuuu');
}

const bobby = new Perro('Bobby','Macho','Mediano');

console.log(bobby);

bobby.ladrar();
bobby.sonar();