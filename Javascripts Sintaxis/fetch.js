// Fetch
// GET -> Traer informacion
// POST -> Enviar Información
// PUT -> Actualizar Información
// DELETE -> Eliminar Información

function ObtenerDatosLicitacionesGeneral(){
    fetch(
        'http://api.mercadopublico.cl/servicios/v1/publico/licitaciones.json?fecha=27112020&ticket=F8537A18-6766-4DEF-9E59-426B4FEE2844',
        {method:'GET'},
        ).then(function(respuesta){
            return respuesta.json()
        }).then(function(respuestaParseada){
            console.log(respuestaParseada)
        });
}

function ObtenerDatosLicitacionesDetallada(licitacion){
    fetch(
        'http://api.mercadopublico.cl/servicios/v1/publico/licitaciones.json?codigo='+licitacion+'&ticket=F8537A18-6766-4DEF-9E59-426B4FEE2844',
        {method:'GET'},
        ).then(function(respuesta){
            return respuesta.json()
        }).then(function(respuestaParseada){
            console.log(respuestaParseada)
        });
}