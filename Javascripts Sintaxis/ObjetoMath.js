// Obtener el numero absoluto
console.log(Math.abs(-7.8));
// Redondea hacia arriba siempre
console.log(Math.ceil(7.2));
// Redondea hacia abajo siempre
console.log(Math.floor(7.8));
// Redondea normal
console.log(Math.round(7.49));
// raiz cuadrada de un numero
console.log(Math.sqrt(81));
// Exponencial Numeros (Base,Exponente)
console.log(Math.pow(2,5));
// ver si un numero es positivo o negativo(cuando es negativo es -1 y cuando es positivo es 1)
console.log(Math.sign(-5));
// Entrega un valor aleatorio entre 0 y 1  -> si se requiere un numero aleatorio entre 1 y  100 deberia ser console.log(Math.round()*100);
console.log(Math.round());
