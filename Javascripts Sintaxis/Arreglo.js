// Definimos el arreglo
var arreglo= ['Cero','Uno','Dos','Tres','Cuatro','Cinco','Seis','Siete'];

// Recorremos el arreglo con forEach

arreglo.forEach(function(valor,indice){
    console.log('Valor : ', valor,' ,Indice : ', indice);
});

// Agregar elementos al arreglo

arreglo.push('ocho');
console.log('Se agrego')

arreglo.forEach(function(valor,indice){
    console.log('Valor : ', valor,' ,Indice : ', indice);
});

// Eliminar elementos de arreglo
arreglo.pop()