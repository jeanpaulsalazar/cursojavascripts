function sumar(a,b){
    return a + b;
}

function restar(a,b){
    return a - b;
}
// Se puede exportar como objeto las funciones y no exportar todo
export const aritmetica = {
    sumar,
    restar
};