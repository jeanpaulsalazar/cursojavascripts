console.log(this);
this.lugar="Contexto Global ";

function saludar(saludo,aQuien){
    console.log(`${saludo} ${aQuien} desde el ${this.lugar}`);
}

saludar("Hola","Jean");

const obj = {
    lugar:"contexto Objeto"
}

saludar.call(obj,"Hola","Jean");
saludar.call(null,"Hola","Jean");
saludar.call(this,"Hola","Jean");

saludar.apply(obj,["Hola","Jean"]);
saludar.apply(null,["Hola","Jean"]);
saludar.apply(this,["Hola","Jean"]);

this.nombre="window";

const persona ={
    nombre: "Jean",
    saludar:function(){
        console.log(`Hola ${this.nombre}`)
    }
}


persona.saludar()

const otraPersona ={
    saludar:persona.saludar.bind(this)
}

otraPersona.saludar();