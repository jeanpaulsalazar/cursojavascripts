/*
Symbol es un tipo de datos cuyos valores son únicos e immutables. 
Dichos valores pueden ser utilizados como identificadores (claves) de las propiedades de los objetos.  
Cada valor del tipo Symbol tiene asociado un valor del tipo String o Undefined que sirve únicamente como descripción del símbolo.
https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Objetos_globales/Symbol
*/
let id  = Symbol("id");
let id2 = Symbol("id2");

console.log(id===id2);
console.log(id,id2);
console.log(typeof id, typeof id2);

// La buena practica nos indica que si es una constante debe ir en mayuscula
const NOMBRE = Symbol();

// El symbol nos permite crear un identificador unico  y privado en nuestros objetos

const persona = {
    [NOMBRE]: "jean"
};

// en la impresion se aprecia solo el dato y no su propiedad
console.log(persona);

//Ejemplo contrario
persona.NOMBRE = "Jean Paul Salazar Yañez"
console.log(persona);
console.log(persona.NOMBRE);
console.log(persona[NOMBRE]);
//

