class Personaje{
    //El constructor es un método especial que se ejecuta en el momento de instanciar la clase
    constructor(nombre,genero){
        this.nombre = nombre;
        this.genero = genero;
    }

    // Metodos
    sonar(){
        console.log('Hago sonidos  porque quiero');
    }

    saludar(){
        console.log(`Hola me llamo ${this.nombre}`);
    }

}

class SuperHeroe extends Personaje{
    constructor(nombre,genero,tamano){
        //Con el método super() se manda a llamar el constructor de la clase padre
        super(nombre,genero);
        this.tamano = tamano;
        this.raza = null;
    }
    sonar(){
        console.log('Soy un super Perro y mi sonido es una ladrido');
    }

    ladrar(){
        console.log('Guuuau Guuuuau')
    }

    // Metodo Estatico (Este metodo lo puedo ejecutar sin la necesidad de haber creado un objeto)
    static queEres(){
        console.log('Soy un super perro capaz de destruir el munto');
    }

    //  Los setters y getters son métodos especiales que nos permiten establecer y obtener los valores de atributos de nuestra clase

    get getRaza(){
        return this.raza;
    }

    set setRaza(raza){
        this.raza = raza;
    }
}

SuperHeroe.queEres();

const mimi    = new  Personaje('Mimi','Hembra'),
      scooby  = new  SuperHeroe('Scooby', 'Macho' ,'Gigante');

console.log(mimi);
console.log(scooby);
scooby.sonar();
scooby.ladrar();

// metodo obtenedor (get)
console.log(scooby.getRaza);

// Metodo asignador (set)
scooby.setRaza = 'Gran Danés';
console.log(scooby.getRaza);
