

function cuadradoPromise(valor){
    if(typeof valor !== "number"){
    return Promise.reject(`Error, el valor ${valor} ingresado no es un número`);
}   
    return new Promise((resolve,reject)=>{
        setTimeout(() => {
            resolve({
                valor,
                result: valor*valor
            });
        }, 0|Math.random()*1000);

    });
}
// Para crear funciones asincronas se antepone async

async function funcionAsincronaDeclarada(){
    try {
        console.log('Inicio Async  Function');

        let obj = await cuadradoPromise(0);
        console.log(`Async Function: ${obj.valor},${obj.result}`);
        obj = await cuadradoPromise(1);
        console.log(`Async Function: ${obj.valor},${obj.result}`);
        obj = await cuadradoPromise(2);
        console.log(`Async Function: ${obj.valor},${obj.result}`);
        obj = await cuadradoPromise(3);
        console.log(`Async Function: ${obj.valor},${obj.result}`);
        obj = await cuadradoPromise(4);
        console.log(`Async Function: ${obj.valor},${obj.result}`);
        obj = await cuadradoPromise(5);
        console.log(`Async Function: ${obj.valor},${obj.result}`);
        console.log('Fin Async  Function');
    } catch (error) {
        
    }
}

funcionAsincronaDeclarada();