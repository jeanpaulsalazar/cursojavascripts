// Objetos

var superheroe = {
    setNombre: function(nombre){
        this.nombre = nombre;
    },
    setSuperpoder: function(superpoder){
        this.superpoder = superpoder;
    },
    setFuerza: function(fuerza){
        this.fuerza = fuerza;
    },
    setDefensa: function(defensa){
        this.defensa = defensa;
    },
    //superheroe.setTipo('Aire')
    setTipo: function(tipo){
        this.tipo = tipo;
    },
    //superheroe.golpes[1].length
    golpes:['Electrico','Piedra'],
    //superheroe.movimiento.Electrico
    movimiento: {
        Electrico : 'Fuerza thor',
        Piedra: 'Martillo'
    }
    // Object.values(superheroe)
    // Object.keys(superheroe)
    // Object.entries(superheroe)


   
}
    // Congelar los valores de un objeto
var superheroe1 = Object.freeze(superheroe);