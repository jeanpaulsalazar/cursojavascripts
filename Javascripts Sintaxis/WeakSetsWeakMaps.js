/*
Los objetos WeakSet son colecciones de objetos. 
Un objecto en WeakSet solo puede ser agregado una vez; 
Esto quiere decir que es unico en la coleccion WeakSet.

No podemos recorrerlos
*/

const WS = new WeakSet();

let valor1 = {"Valor1":1};
let valor2 = {"Valor2":2};
let valor3 = {"Valor3":3};
let valor4 = {"Valor4":4};

WS.add(valor1);
WS.add(valor2);
WS.add(valor3);
WS.add(valor4);

console.log(WS);

setInterval(() => {
   console.log(WS); 
}, 5000);

// Asignamos null y el recoletor de basura limpia de forma automatica el weakset
setTimeout(()=>{
    valor1 = null;
    valor2 = null;
    valor3 = null;
    valor4 = null;
},3000);


/*
Las claves de los WeakMaps solamente pueden ser del tipo Object. 
Los Primitive data types como claves no están permitidos (ej. un Symbol no pueden ser una clave de WeakMap).
*/

const Weakmp = new WeakMap();

let llave1 = {"Hola":1};
let llave2 = {};
let llave3 = {};

Weakmp.set(llave1,1);

console.log(Weakmp);