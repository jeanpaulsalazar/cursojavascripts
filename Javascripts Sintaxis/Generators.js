/*

 yield* [[expression]];

La expresión yield* itera sobre el operador realizando yield de cada valor retornado por este.

El valor de la expresion yield* es el valor retornado por el iterador en si mismo cuando es finalizado (ej., cuando done es true).

*/
let numero  = 2
let numero1 = 3
let numero2 = 4

function* g1() {
    yield numero;
    yield numero1;
    yield numero2;
  }
  numero =  numero *2;
  var iterator = g1();

for (let  y of iterator) {
    console.log(y);
}

  