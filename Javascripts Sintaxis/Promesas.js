// Promesas
/*
var promise = new Promise(function(resolve, reject){
    if(){
        resolve();
    }else{
        reject();
    }
})

Se Ejecuta cuando la promesa se cumple
promise.then(function(resultado){
    console.log(resultado);
})

Se ejecuta cuando la promesa no se cumple
promise.catch(function(error){
    console.log(error);
})

*/

function resolveAfter2Seconds(x) { 
    return new Promise(resolve => {
      setTimeout(() => {
        resolve(x);
      }, 2000);
    });
  }
  
  async function f1() {
    var x = await resolveAfter2Seconds(10);
    console.log(x); // 10
  }
  f1();