/*

Procesamiento Single thread (Un solo Hilo) y Multi thread. // Javascripts trabaja en lifo
Pagina que permite ver  la pila de procesos de codigo javascripts
http://latentflip.com/loupe/


*/

// Ejemplo de codigo sincronono Bloqueante

(()=>{
    console.log("Codigo Sincrono");
    console.log("Inicio");

    function dos(){
        console.log("Dos");
    }

    function uno(){
        console.log("Uno");
        dos();
        console.log("Tres");
    }

    uno();
    console.log("Fin");
})();

// Ejemplo de codigo no bloqueante Asincrono

(()=>{
    console.log("Codigo Asincrono");
    console.log("Inicio");

    function dos(){
       setTimeout(() => {
        console.log("Dos");
       }, 1000);
    }

    function uno(){
        setTimeout(() => {
        console.log("Uno");
        }, 0);
        dos();
        console.log("Tres");
    }

    uno();
    console.log("Fin");
})();
