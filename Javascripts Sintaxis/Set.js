/*
Los objetos Set son colecciones de valores. 
Se puede iterar sus elementos en el orden de su inserción. 
Un valor en un Set sólo puede estar una vez; éste es único en la colección Set.
*/

const set = new Set([
    1,2,3,4,5,6,6,true,false,false,{},{},"Hola","hola"
]);
console.log(set);

// Agregar valores al set

const set2 = new Set();
set2.add(1);
set2.add(2);
set2.add(3);
set2.add(4);
set2.add(4);
set2.add(13);

console.log(set2);

// Para eliminar valores del set

set2.delete(4);
console.log(set2);

// Recorrer SET mediante un for of

for (item of set) {
    console.log(item);
}

// Permite validar si un dato se encuentra en dentro de un set

console.log(set2.has(1));
console.log(set2.has(19));

// metodo que permite limpiar un set completo

set2.clear();
console.log(set2);

