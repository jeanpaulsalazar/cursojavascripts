export const PI = Math.PI;

export let usuario = "Jean";
let password = "querty";

//Exportar por defecto(Solo puede tener 1) (Solo se permite con funciones y con clases)
export default function saludar(){
    console.log("Hola Módulos  +ES6");
}

// Exportando clases

export class Saludar{
    constructor(){
        console.log("Hola Clases +ES6");
    }
};