// Ejemplo Antiguo
let nombre  =   'Jean',
    edad    =   10;

const perro ={
    nombre:nombre,
    edad:edad,
    ladrar:function(){
        console.log("Guau Guau")
    }
};

console.log(perro);
perro.ladrar();

// Ejemplo de objeto literal

const dog ={
    nombre,
    edad,
    raza:'Callejero',
    ladrar(){
        console.log('Guau Guau Guau')
    }
};

console.log(dog);