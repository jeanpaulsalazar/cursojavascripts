
function cuadradoPromise(valor){
    if(typeof valor !== "number"){
    return Promise.reject(`Error, el valor ${valor} ingresado no es un número`);
}   
    return new Promise((resolve,reject)=>{
        setTimeout(() => {
            resolve({
                valor,
                result: valor*valor
            });
        }, 0|Math.random()*1000);

    });
}

cuadradoPromise(0)
.then((obj)=>{
    //console.log(obj);
    console.log('Inicia Promise');
    console.log(`Promise: ${obj.valor},${obj.result}`);
    return cuadradoPromise(1);
})
.then(obj=>{
    console.log(`Promise: ${obj.valor},${obj.result}`);
    return cuadradoPromise(2);
})
.then(obj=>{
    console.log(`Promise: ${obj.valor},${obj.result}`);
    return cuadradoPromise(3);
})
.then(obj=>{
    console.log(`Promise: ${obj.valor},${obj.result}`);
    return cuadradoPromise("a");
})
.then(obj=>{
    console.log(`Promise: ${obj.valor},${obj.result}`);
    return cuadradoPromise(5);
})
.then(obj=>{
    console.log(`Promise: ${obj.valor},${obj.result}`);
    console.log('Fin Promise');
})
.catch(err => console.error(err));

funcionAsincronaDeclarada();