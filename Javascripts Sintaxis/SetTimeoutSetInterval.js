// SetTimeout-> Revise callback y un tiempo expresado en Milisegundos// Se ejecuta una sola vez
setTimeout(()=>{
console.log("Hola")
},1000);

// SetInterval -> Recibe una Funcion y se ejecutara cada x tiempo segun se define

setInterval(() => {
    console.log("Ejecutando un setInteval, esto se ejecuta indefinidamente cada cierto tiempo");
}, 3000);

// Existe una forma de cancelar un setTimeOut Pero el timeout debe ser almacenado en una variable.

let temporizador = setTimeout(()=>{
    console.log("Detener time out")
    },1000);

// Revise la variable donde se almaceno el timeout

clearTimeout(temporizador);


// Existe una forma de cancelar un setInterval Pero el timeout debe ser almacenado en una variable.

let temporizadorIntervalo = setInterval(()=>{
    console.log("Detener interval")
    },1000);

// Revise la variable donde se almaceno el timeout

clearInterval(temporizadorIntervalo);